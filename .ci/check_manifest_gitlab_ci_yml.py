#!/usr/bin/env python
import yaml
with open('manifest-gitlab-ci.yml', 'rt') as f:
    print(yaml.load(f.read(), Loader=yaml.Loader))
