# FSL release manifest rules

This repository contains CI configuration and scripts which are used in the
fsl/conda/manifest> repository to generate, test, and deploy FSL release
manifest and environment files. Refer to the fsl/conda/manifest> repository
for more details.


These scripts are hosted separately from the `fsl/conda/manifest` project so
that they can be updated independently of FSL releases.


Whenever changes to the contents of this repository are made, the version
number in `manifest_rules/__init__.py` must be incremented.
