#!/usr/bin/env python
#
# Remove files from the release directory according to a glob-style wildcard
# pattern.  Can be used for development, experimentation, maintenance, or
# when mistakes happen.
#
# The PATTERN variable must be set to one or more glob- style wildcard
# patterns, separated with semi-colons, denoting the files that are to be
# removed.  The DRY_RUN variable must be explicitly set to "false" for any
# files to actually be deleted.
#


import os.path as op
import            os
import            sys
import            glob

from manifest_rules.utils import create_devrelease_index


def main():
    release_dir = op.abspath(sys.argv[1])
    patterns    =            sys.argv[2].split(';')
    dry_run     =            sys.argv[3] != 'false'

    print('Clearing release files')
    print(f'  release dir: {release_dir}')
    print(f'  patterns:    {patterns}')
    print(f'  dry_run:     {dry_run}')

    for pattern in patterns:
        pattern = op.join(release_dir, pattern.strip())
        matches = glob.glob(pattern)

        for match in matches:
            print(f'Removing: {match}')
            if not dry_run:
                os.remove(match)

    create_devrelease_index(release_dir)


if __name__ == '__main__':
    sys.exit(main())
