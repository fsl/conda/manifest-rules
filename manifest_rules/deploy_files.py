#!/usr/bin/env python
#
# Deploy environment/manifest files to the release directory.
#

import os
import os.path as op
import sys
import shutil

from manifest_rules.utils import create_devrelease_index


def main():

    dest   = sys.argv[1]
    files  = sys.argv[2:]
    branch = os.environ.get('CI_COMMIT_BRANCH', None)
    tag    = os.environ.get('CI_COMMIT_TAG',    None)

    if tag is not None:
        print(f'Publishing files from tag {tag}')
    else:
        print(f'Publishing files from branch {branch}')

    for src in files:
        print(f'Copying {src} to {dest}')
        shutil.copy(src, dest)

    # If this is a release, make manifest.json
    # a copy of manifest-<tag>.json (the
    # server which serves the releases folder
    # doesn't support sym-links). The
    # manifest-<tag>.json files are retained
    # for archival/roll-back purposes.
    if tag is not None:
        mainfile    = op.join(dest,  'manifest.json')
        releasefile = op.join(dest, f'manifest-{tag}.json')

        if not op.exists(releasefile):
            print(f'Release manifest file does not exist! {releasefile}')
            sys.exit(1)

        if op.exists(mainfile):
            os.remove(mainfile)

        shutil.copy(releasefile, mainfile)

    create_devrelease_index(dest)


if __name__ == '__main__':
    main()
