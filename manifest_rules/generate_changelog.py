#!/usr/bin/env python
#
# generate_changelog.py - Generate a changelog for a FSL release.
#


import              datetime
import              fnmatch
import functools as ft
import              json
import              re
import os.path   as op
import              os
import              sys
import textwrap  as tw
import              yaml

from fsl_ci.conda         import (get_project_repository_and_revision,
                                  load_meta_yaml)
from fsl_ci.gitlab        import (format_gitlab_date,
                                  parse_gitlab_date,
                                  gen_project_path,
                                  get_commits,
                                  get_default_branch,
                                  get_divergence,
                                  get_merge_request,
                                  get_tag_info,
                                  lookup_project_tags,
                                  download_file as gitlab_download_file)
from manifest_rules.utils import  sprun, Version, download_file, tempdir


# Don't include these packages in the change log
IGNORE = [
    'boost-cpp',
    'c-compiler',
    'cxx-compiler',
    'ghalton',
    'hlsvdpropy',
    'libmamba',
    'libopenblas',
    'make',
    'nibabel',
    'nidmresults',
    'nidmresults-fsl',
    'matplotlib',
    'matplotlib-base',
    'numpy',
    'pytorch',
    'openblas',
    'openslide-python',
    'pkg-config',
    'python',
    'scikit-learn',
    'tk',
    'vtk',
    'vtk-base',
    'wxpython',
    'wxwidgets',
    'xgboost'
    'zlib'
]

# URLs for some projects which cannot be automatically
# determined from the project name, i.e. which don't follow
# the pattern:
#  - name:         <name>
#  - project repo: fsl/<name>
#  - recipe repo:  fsl/conda/fsl-<name>
#
URLS = {
    'fsl-pipe'           : 'https://git.fmrib.ox.ac.uk/fsl/fsl-pipe.git',
    'fsl-pipe-gui'       : 'https://git.fmrib.ox.ac.uk/ndcn0236/fsl-pipe-gui.git',
    'file-tree'          : 'https://git.fmrib.ox.ac.uk/fsl/file-tree.git',
    'file-tree-fsl'      : 'https://git.fmrib.ox.ac.uk/fsl/file-tree-fsl.git',
    'fmrib-unpack'       : 'https://git.fmrib.ox.ac.uk/fsl/funpack.git',
    'fsleyes'            : 'https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes.git',
    'fslpy'              : 'https://git.fmrib.ox.ac.uk/fsl/fslpy.git',
    'spec2nii'           : 'https://github.com/wtclarke/spec2nii.git',
    'fsleyes-plugin-mrs' : 'https://git.fmrib.ox.ac.uk/wclarke/fsleyes-plugin-mrs.git',
    'truenet'            : 'https://github.com/v-sundaresan/truenet.git',
}


def sanitise_package_name(name):
    match = re.match(r'(.*-cuda)-(\d+\.\d+|X\.Y)$', name)
    if match:
        name = match.group(1).removesuffix('-cuda')
    return name


@ft.cache
def get_project_repo(project_name, server, token):

    url = URLS.get(project_name, None)

    if url is not None:
        return url

    url = f'https://git.fmrib.ox.ac.uk/fsl/conda/{project_name}.git'

    with tempdir():

        if server in url:
            project_path = gen_project_path(url)
            meta         = gitlab_download_file(project_path, 'meta.yaml', server, token)

            with open('meta.yaml', 'wt') as f:
                f.write(meta)
            repo = get_project_repository_and_revision('meta.yaml')[0]

    return repo


@ft.cache
def get_tags(project_name, server, token):
    """Get all tags for a project. """

    url = get_project_repo(project_name, server, token)

    # we can use gitlab API for internal projects
    if server in url:
        project_path = gen_project_path(url)
        tags         = lookup_project_tags(project_path, server, token)

    # We have to clone external projects
    else:
        # one line per tag in the form
        # 0c22f0d001d2f32572a8fc96e99ba16603afc52c        refs/tags/v1.0.0
        tags = sprun(f'git ls-remote {url} --tags "*"', text=True).strip().split('\n')
        tags = [t.split()[1]          for t in tags]
        tags = [t                     for t in tags if t.startswith('refs/tags/')]
        tags = [t.split('/')[2]       for t in tags]
        tags = [t.removesuffix('^{}') for t in tags]
        uniq = []
        for t in tags:
            if t not in uniq:
                uniq.append(t)
        tags = uniq

    return tags


def normalise_version(project_name, version, server, token):
    """Find the git tag corresponding to the given version.

    Some projects have version tags beginning with a "v", but package versions
    without the v, and vice versa.

    Some projects are unpinned, e.g. "1.8.*" - we try to find the closest match
    from available tags.
    """

    orig_tags    = get_tags(project_name, server, token)
    tags         = list(orig_tags)
    orig_version = version

    if version.lower().startswith('v'):
        version = version[1:]

    for i, tag in enumerate(tags):
        if tag.lower().startswith('v'):
            tags[i] = tag[1:]

    for i, tag in enumerate(tags):
        if version in tag:
            return orig_tags[i]

    # try matching glob, going in
    # reverse so we encounter the
    # most recent match first
    for i, tag in enumerate(tags):
        if fnmatch.fnmatch(tag, version):
            return orig_tags[i]

    return orig_version


@ft.cache
def get_commits_between(project_name, oldver, newver, server, token):
    """get commits between oldver and newver."""

    url = get_project_repo(project_name, server, token)

    oldver = normalise_version(project_name, oldver, server, token)
    newver = normalise_version(project_name, newver, server, token)

    # we can use gitlab API for internal projects
    if server in url:
        project_path = gen_project_path(url)
        commits      = get_divergence(project_path, server, token, oldver, newver)[0]
        commits      = filter_commits(commits)

        # get_divergence may return spurious commits for
        # projects such as FSLeyes which create releases
        # off different branches. So we apply a primitve
        # threshold to remove commits older than oldver.
        oldinfo = get_tag_info(project_path, server, token, oldver)
        olddate = parse_gitlab_date(oldinfo['commit']['created_at'])
        commits = [c for c in commits if parse_gitlab_date(c['created_at']) > olddate]

        # Also, for fsleyes/fslpy projects which use
        # release branches, the commit SHAs returned
        # by get_divergence may not match the commit
        # SHAs on the default branch. So here we
        # replace the commits with matching ones from
        # the default branch.
        commits = find_origin_commits(commits, project_path, server, token, oldver, newver)

        # turn into {sha:msg} dict
        commits = {c['id'] : c['message'].strip() .replace('\n', ' ')
                   for c in commits}

    # We have to clone external projects
    else:
        with tempdir():
            env                                = os.environ.copy()
            env['GIT_LFS_SKIP_SMUDGE']         = '1'
            env['GIT_CLONE_PROTECTION_ACTIVE'] = 'false'
            sprun(f'git clone {url} project', env=env)
            commits  = sprun(f'git log --format="%H %s" {oldver}..{newver}',
                             text=True, cwd='project').strip().split('\n')
            commits  = dict(c.split(' ', 1) for c in commits)

    # Strip merge commits
    commits = {sha : msg for sha,msg in commits.items()
               if not msg.startswith("Merge branch")}

    return commits


def find_origin_commits(commits, project_path, server, token, oldver, newver):
    """Given a list of commits, attempts to find the corresponding original commit
    from the repository default branch, in the event that the commit was
    from a merge or cherry-pick.
    """

    olddate = get_tag_info(project_path, server, token, oldver)['commit']['created_at']
    newdate = get_tag_info(project_path, server, token, newver)['commit']['created_at']
    history = get_commits( project_path, server, token, from_=olddate, to=newdate)
    history = filter_commits(history)

    origin_commits = []

    for commit in commits:
        for ocommit in history:
            if commit['message'] == ocommit['message']:
                origin_commits.append(ocommit)
                break

    return origin_commits


def filter_commits(commits):

    patterns = [
        'mnt: version',
        'version',
        'doc: changelog',
        'changelog',
        'update changelog',
        'update changelog.',
        'merge branch *',
        'lint',
        'update doc',
        'update doc.',
        'typo',
    ]

    filtered = []

    for commit in commits:
        nmsg = commit['message'].strip().lower()
        if not any(fnmatch.fnmatch(nmsg, p) for p in patterns):
            filtered.append(commit)

    return filtered


def get_merge_requests(project_name, oldver, newver, server, token):

    # MR messages currently only retrieved
    # for internally hosted projects.
    url = get_project_repo(project_name, server, token)
    if server not in url:
        return {}

    oldver       = normalise_version(project_name, oldver, server, token)
    newver       = normalise_version(project_name, newver, server, token)
    project_path = gen_project_path(url)
    defbranch    = get_default_branch(project_path, server, token)

    # There are some awkward limitations
    # in the gitlab REST API...
    #
    # We can't get the date that a tag was created,
    # only the date of the associated commit :(
    #
    # And it's not currently possible to filter MRs
    # by their merge date - we can only filter
    # by creation date :(.
    #
    # So instead of querying merge requests and
    # filtering them by date, we get all MRS
    # associated with each commit
    commits = get_commits_between(project_name, oldver, newver, server, token)
    mrs     = [get_merge_request(project_path, server, token, commit=sha)
               for sha in commits.keys()]
    mrs     = [mr for mr in mrs if mr is not None]
    mrs     = [mr for mr in mrs if mr['target_branch'] == defbranch]
    mrs     = sorted(mrs, key=lambda mr: mr['iid'])
    mrs     = {mr['iid'] : mr for mr in mrs}

    for iid, mr in list(mrs.items()):

        title = mr['title']
        desc  = mr['description']
        url   = mr['web_url']

        if desc.strip() == '':
            desc = None

        mrs[iid] = (title, desc, url)

    return mrs


def load_env_file(manifest, fslversion, platform):
    env_files = manifest['versions'][fslversion]
    for env_file in env_files:
        if env_file['platform'] == platform:
            break
    else:
        raise ValueError(f'Could not find YAML file for FSL version '
                         f'{fslversion} and platform {platform}!')

    with tempdir(change=False) as td:
        envfile = op.join(td, 'env.yaml')
        download_file(env_file['environment'], envfile)
        with open(envfile, 'rt') as f:
            packages = yaml.load(f.read(), Loader=yaml.Loader)['dependencies']

    # turn into a {name : version} dictionary
    pkgdict = {}
    for package in packages:
        package = package.split(' ')
        if len(package) == 1:
            name = package[0]
            ver  = None
        else:
            name, ver = package[:2]

        name = sanitise_package_name(name)
        pkgdict[name] = ver

    return pkgdict


def generate_changelog(fslversion, old_packages, new_packages, destfile, server, token):

    added   = set(new_packages.keys()).difference(old_packages.keys())
    removed = set(old_packages.keys()).difference(new_packages.keys())
    changed = []

    for pkg in list(added):
        if pkg in IGNORE:
            print(f'Ignoring package {pkg}')
            added.remove(pkg)
            continue
        new_packages.pop(pkg)

    for pkg in list(removed):
        if pkg in IGNORE:
            print(f'Ignoring package {pkg}')
            removed.remove(pkg)
            continue

    for pkg in list(new_packages.keys()):

        if pkg in IGNORE:
            print(f'Ignoring package {pkg}')
            continue

        oldver = old_packages[pkg]
        newver = new_packages[pkg]

        if oldver is None or newver is None:
            # print(f'Version missing for {pkg} [old: {oldver}, new: {newver}]')
            continue
        if oldver == newver:
            # print(f'Project {pkg} has not changed [{newver}] - skipping')
            continue

        print(f'Project {pkg} has changed [{oldver} -> {newver}]')
        changed.append(pkg)

    def suffix(d):
        if d in (1, 21, 31): return 'st'
        if d in (2, 22):     return 'nd'
        if d in (3, 23):     return 'rd'
        return 'th'

    date      = datetime.date.today()
    dsuf      = suffix(date.day)
    date      = date.strftime(f'%-d{dsuf} %B %Y')
    changelog = [f'# FSL {fslversion}, {date}\n\n']
    changelog.extend([f'- **New** {pkg}:\n' for pkg in sorted(added)])
    changelog.extend([f'- Removed {pkg}:\n' for pkg in sorted(removed)])

    # toc
    for pkg in changed:
        oldver = old_packages[pkg]
        newver = new_packages[pkg]
        changelog.append(f'- [**{pkg}** {oldver} -> {newver}](#{pkg})\n')

    for pkg in changed:

        oldver = old_packages[pkg]
        newver = new_packages[pkg]

        changelog.append(f'\n## <a id="{pkg}" href="#{pkg}">{pkg} {oldver} -> {newver}</a>\n\n')

        try:
            commits = get_commits_between(pkg, oldver, newver, server, token)
        except Exception as e:
            print(f'Error retrieving commit history for package {pkg}: {e}')
            commits = {}
        try:
            mrs = get_merge_requests(pkg, oldver, newver, server, token)
        except Exception as e:
            print(f'Error retrieving merge request history for package {pkg}: {e}')
            mrs = {}

        for title, desc, url in mrs.values():
            changelog.append(f'- [*{title}*]({url})\n')
            if desc is not None:
                changelog.append(tw.indent(desc, '  ') + '\n')

        if len(commits) > 0:
            changelog.append('\n**Detailed history**\n\n')
            for msg in commits.values():
                msg = msg.replace('`', '')
                msg = msg.replace('\n', '  ')
                changelog.append(f'- `{msg}`\n')

    with open(destfile, 'wt') as f:
        f.writelines(changelog)


def main():

    if len(sys.argv) not in (3, 4):
        print('Usage: generate_changelog oldmanifest '
              'newmanifest [environment_file_directory]')
        print('  The manifest paths can be URLs or paths to local files')
        print('  If the new manifest refers to environment files (yams) '
              'that have not yet been published, you must specify a local '
              'directory which contains them')
        sys.exit(1)

    oldmanifest = sys.argv[1]
    newmanifest = sys.argv[2]
    token       = os.environ['FSL_CI_API_TOKEN']
    server      = os.environ['CI_SERVER_URL']

    if len(sys.argv) == 4: envdir = op.abspath(sys.argv[3])
    else:                  envdir = None

    with tempdir(change=False) as td:
        oldfile = op.join(td, 'old.json')
        newfile = op.join(td, 'new.json')
        download_file(oldmanifest, oldfile)
        download_file(newmanifest, newfile)
        with open(oldfile, 'rt') as f:
            oldmanifest = json.loads(f.read())
        with open(newfile, 'rt') as f:
            newmanifest = json.loads(f.read())

    oldversion = oldmanifest['versions']['latest']
    newversion = newmanifest['versions']['latest']
    outfile    = f'FSL-{newversion}-changelog.md'

    # patch the new manifest to refer to local environment files -
    # manifests are populated with URLs to environment files, but
    # they may not have been published yet
    if envdir is not None:
        for build in newmanifest['versions'][newversion]:
            envfname = build['environment'].split('/')[-1]
            build['environment'] = op.join(envdir, envfname)

    print(f'Creating change log for FSL release {oldversion} -> {newversion}')

    # We compare linux-64 env files, as all
    # packages are generally equivalent across
    # platforms, but linux-64 additionally
    # contains CUDA packages
    old_packages = load_env_file(oldmanifest, str(oldversion), 'linux-64')
    new_packages = load_env_file(newmanifest, str(newversion), 'linux-64')

    generate_changelog(newversion, old_packages, new_packages, outfile, server, token)


if __name__ == '__main__':
    main()
