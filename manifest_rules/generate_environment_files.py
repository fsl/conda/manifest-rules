#!/usr/bin/env python
#
# Generate FSL conda environment files from fsl-release.yml
#
# An environment file is created for all supported platforms.  Platform
# identifiers are taken from the FSLCONDA_PLATFORMS CI / CD environment
# variable if set, or taken from the entries in the miniconda field from
# fsl-release.yml.
#


import itertools as it
import os.path   as op
import              os
import              sys

from fsl_ci.conda         import  get_channel_packages
from manifest_rules.utils import (load_release_info,
                                  get_platform_identifiers,
                                  generate_environment_file_name)


def generate_package_list(release_info, platform, env_name=None):
    """Return a dict of {name : version} pairs denoting packages that should be
    included in the environment file for the platform. The version may be None.

    If env_name is None, the main environment package list is returned.
    Otherwise a package list for the extra/child env_name environment
    is returned.
    """

    if env_name is None:
        pkgs     = release_info['packages']
        platpkgs = release_info.get(f'{platform}-packages', [])
        pkgs     = pkgs + platpkgs
    else:
        pkgs     = release_info['extras'][env_name]

    # "cuda" may be present as a dependency -
    # this is not a package name, but a hint
    # that the environment contains packages
    # which may take advantage of CUDA. For
    # these environments, the next CI stage
    # (test_environment) will add a
    # "cuda_enabled" flag to the manifest
    # entry, which will then be used by the
    # fslinstaller.
    if 'cuda' in pkgs:
        pkgs.remove('cuda')

    pkgdict = {}

    for pkg in pkgs:
        pkg = pkg.split(maxsplit=1)
        if len(pkg) == 2: pkg, ver = pkg
        else:             pkg, ver = pkg[0], ''

        pkgdict[pkg] = ver

    return pkgdict


def need_internal_channel(release_info, packages):
    """Returns True if any of the given packages are sourced from the
    internal FSL conda channel.
    """

    username          = os.environ['FSLCONDA_USERNAME']
    password          = os.environ['FSLCONDA_PASSWORD']
    channel_url       = release_info['internal-channel']
    channel_url       = channel_url.replace('${FSLCONDA_USERNAME}:${FSLCONDA_PASSWORD}@', '')
    internal_packages = get_channel_packages(channel_url,
                                             username=username,
                                             password=password)
    internal_packages = list(internal_packages.keys())
    packages          = [p.split()[0] for p in packages]

    return any(p in internal_packages for p in packages)


def generate_environment(release_info, platform, outfile):
    """Genereate the primary FSL environment file for the platform. """
    channels = list(release_info['channels'])
    packages = generate_package_list(release_info, platform)

    # Internal/dev release - add internal
    # channel to environment spec if necessary
    if need_internal_channel(release_info, packages):
        channels = [release_info['internal-channel']] + channels

    with open(outfile, 'wt') as f:
        f.write('name: FSL\n')
        f.write('channels:\n')
        for channel in channels:
            f.write(f' - {channel}\n')
        f.write('dependencies:\n')
        for pkg, ver in packages.items():
            f.write(f' - {pkg} {ver}\n')


def generate_extra_environment(release_info, env_name, platform, outfile):
    """Generate an environment file for a child/extra environment,
    for the platform.
    """
    channels = list(release_info['channels'])
    mainpkgs = generate_package_list(release_info, platform)
    envpkgs  = generate_package_list(release_info, platform, env_name)

    # Internal/dev release - add internal
    # channel to environment spec if necessary
    internal_channel = release_info['internal-channel']
    if need_internal_channel(release_info, envpkgs) or \
       need_internal_channel(release_info, mainpkgs):
        channels = [internal_channel] + channels

    allpkgs = {}

    # Retrieve metadata about all
    # internally hosted packages
    for channel in reversed(channels):
        if not channel.startswith('https:'):
            continue
        if channel == internal_channel:
            username = os.environ['FSLCONDA_USERNAME']
            password = os.environ['FSLCONDA_PASSWORD']
            channel  = channel.replace('${FSLCONDA_USERNAME}:${FSLCONDA_PASSWORD}@', '')
            chanpkgs = get_channel_packages(channel,
                                            username=username,
                                            password=password)
        else:
            chanpkgs = get_channel_packages(channel)
        allpkgs.update(chanpkgs)
    allpkgs = {pkg : versions[-1] for pkg, versions in allpkgs.items()}

    with open(outfile, 'wt') as f:
        f.write(f'name: {env_name}\n')
        f.write('channels:\n')
        for channel in channels:
            f.write(f' - {channel}\n')
        f.write('dependencies:\n')
        for pkg, ver in envpkgs.items():
            f.write(f' - {pkg} {ver}\n')


def main():
    # Save generated environment files here
    release_file = op.abspath(sys.argv[1])
    outdir       = op.abspath(sys.argv[2])

    # Tags on fsl/conda/manifest denote a
    # public FSL release.  All other
    # commits denote an internal release.
    version      = os.environ.get('CI_COMMIT_TAG', None)
    release_info = load_release_info(release_file)

    platforms    = get_platform_identifiers(release_info)
    extra_envs   = list(release_info.get('extras', {}).keys())

    if not op.exists(outdir):
        os.mkdir(outdir)

    generated = []

    # Generate a primary environment
    # file for all supported platforms.
    for platform in platforms:
        filename = generate_environment_file_name(version, platform)
        filename = op.join(outdir, filename)
        generate_environment(release_info, platform, filename)
        generated.append(('main', platform, op.basename(filename)))

    # Generate environment files for all
    # extra environments (and for each
    # platform).
    for platform, env_name in it.product(platforms, extra_envs):
        filename = generate_environment_file_name(version, platform, env_name)
        filename = op.join(outdir, filename)
        generate_extra_environment(release_info, env_name, platform, filename)
        generated.append((env_name, platform, op.basename(filename)))

    for env, platform, filename in generated:
        print(f'Environment {env}, platform {platform}: {filename}')


if __name__ == '__main__':
    sys.exit(main())
