#!/usr/bin/env python
#
# Replace the current manifest.json with new contents read from the
# MANIFEST_CONTENTS environment variable.
#



import json
import os
import os.path as op
import sys
import shutil


def main():
    releasedir = sys.argv[1]
    dry_run    = os.environ['DRY_RUN'].lower() != "false"
    contents   = os.environ['MANIFEST_CONTENTS']
    manifest   = os.environ['MANIFEST_PATH']
    manifest   = op.join(releasedir, manifest)

    # move manifest.json to manifest.backup.json
    if op.exists(manifest):
        base, ext = op.splitext(manifest)
        backup    = f'{base}.backup{ext}'
        print(f'Moving {manifest} to {backup} '
              f'[dry_run: {dry_run}]')
        if not dry_run:
            shutil.move(manifest, backup)

    # create new manifest
    print(f'Writing new manifest contents to '
          f'{manifest} [dry_run: {dry_run}]')
    print('----------')
    # Not supporting "//"-style comments here
    contents = json.dumps(json.loads(contents), indent=4)
    print(contents)
    print('----------')
    if not dry_run:
        with open(manifest, 'wt') as f:
            f.write(contents)


if __name__ == '__main__':
    main()
