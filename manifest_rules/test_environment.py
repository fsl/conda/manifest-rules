#!/usr/bin/env python
#
# Performs a test installation for a FSL conda environment file. Counts
# the number of output lines produced when installing miniconda, and
# when installing the FSL environment.
#
# When a full test is perforemd, the SKIP_PACKAGES environment variable can be
# used to skip specific packages (e.g. if they are particularly large). It
# can be set to a space-separated list of package names and sizes to be skipped,
# e.g.:
#
#   fsl-data_first_models_317_bin=839804304 fsl-data_first_models_336_bin=1103857693
#
# The package sizes must be specified in bytes, and must be accurate, as they
# are ultimately used by the fslinstaller script to report installation progress.
#


import             contextlib
import             glob
import os.path  as op
import             os
import             sys
import             json
import             shutil
import             tarfile

import yaml

from manifest_rules.utils import (sprun,
                                  clean_environ,
                                  tempdir,
                                  download_file,
                                  sha256,
                                  load_release_info,
                                  get_platform_identifiers,
                                  parse_environment_file_name,
                                  mock_nvidia_smi)


def conda(basedir=None):
    # Prefer micromamba, mamba, conda, in that order
    candidates = []
    if basedir is not None:
        candidates.append(op.join(basedir, 'bin',      'micromamba'))
        candidates.append(op.join(basedir, 'bin',      'mamba'))
        candidates.append(op.join(basedir, 'bin',      'conda'))
        candidates.append(op.join(basedir, 'condabin', 'conda'))
    onpaths = [
        shutil.which('micromamba'),
        shutil.which('mamba'),
        shutil.which('conda')]
    for onpath in onpaths:
        if onpath is not None:
            candidates.append(onpath)
    for condabin in candidates:
        if op.exists(condabin):
            return condabin
    return 'conda'


def get_python_version(envfile):
    """Extract the python version from a FSL conda env.yml file. """
    with open(envfile, 'rt') as f:
        env = f.read()
    env = yaml.load(env, Loader=yaml.Loader)

    packages = env['dependencies']

    # extract first two components of
    # python version (e.g. 3.11)
    pyver = None
    for pkg in packages:
        if pkg.startswith('python '):
            pyver = pkg.split()[-1]
            break
    pyver = '.'.join(pyver.split('.')[:2])

    return pyver


def preprocess_environment(envfile, envdir, extra_pkgs):
    """Make some adjustments to a FSL conda env.yml file. """
    if extra_pkgs is None:
        extra_pkgs = {}
    with open(envfile, 'rt') as f:
        env = f.read()

    # inject internel channel
    # credentials if necessary
    username = os.environ.get('FSLCONDA_USERNAME', 'username')
    password = os.environ.get('FSLCONDA_PASSWORD', 'password')
    env      = env.replace('${FSLCONDA_USERNAME}', username)
    env      = env.replace('${FSLCONDA_PASSWORD}', password)
    env      = yaml.load(env, Loader=yaml.Loader)

    packages = env['dependencies']
    channels = env['channels']

    packages.extend(extra_pkgs)

    with open(op.join(envdir, '.condarc'), 'wt') as f:
        f.write('auto_update_conda: false\n')
        f.write('channel_priority: strict\n')
        f.write('channels:\n')
        for c in channels:
            f.write(f' - {c}\n')

    with open(envfile, 'wt') as f:
        f.write('channels:\n')
        for c in channels:
            f.write(f' - {c}\n')
        f.write('dependencies:\n')
        for p in packages:
            f.write(f' - {p}\n')

    return packages

@contextlib.contextmanager
def calc_progress_metrics(basedir, envdir):
    """Counts the number/sizes of files in the environment directory, used
    as a basis for progress reporting metrics.
    """

    class Results:
        pass

    def listdir(path):
        if op.exists(path): return os.listdir(path)
        else:               return []

    def metrics():
        pkgsdir = op.join(basedir, 'pkgs')
        bindir  = op.join(envdir,  'bin')
        libdir  = op.join(envdir,  'lib')

        pkgs  = listdir(pkgsdir)
        pkgs  = [p for p in pkgs if p.endswith('.tar.bz2') or p.endswith('.conda')]
        pkgs  = sorted(pkgs)
        sizes = [op.getsize(op.join(pkgsdir, p)) for p in pkgs]

        bins = listdir(bindir)
        libs = listdir(libdir)

        return pkgs, len(bins), len(libs), sum(sizes)

    start_pkgs, start_nbins, start_nlibs, start_sizes = metrics()

    results             = Results()
    results.start_pkgs  = start_pkgs
    results.start_npkgs = len(start_pkgs)
    results.start_nbins = start_nbins
    results.start_nlibs = start_nlibs
    results.start_sizes = start_sizes

    yield results

    end_pkgs, end_nbins, end_nlibs, end_sizes = metrics()

    results.end_pkgs  = end_pkgs
    results.end_npkgs = len(end_pkgs)
    results.end_nbins = end_nbins
    results.end_nlibs = end_nlibs
    results.end_sizes = end_sizes
    results.npkgs    =  results.end_npkgs - results.start_npkgs
    results.nbins    =          end_nbins -         start_nbins
    results.nlibs    =          end_nlibs -         start_nlibs
    results.sizes    =          end_sizes -         start_sizes

    return results


def full_test(basedir, envfile, envname, release_info, skip_pkgs, extra_pkgs, env):
    """Full installation test, performed:
       - to make sure that conda is able to resolve the FSL environment files.
       - to generate information about the installation for progress reporting
         during a real installation.
    """

    preprocess_environment(envfile, basedir, extra_pkgs)

    # skip some packages (e.g. data packages,
    # to reduce download requirements)
    skipped = []
    with open(envfile, 'rt') as f, open('env.copy.yml', 'wt') as outf:
        for line in f:
            skip = False
            if line.strip().startswith('-'):
                pkg = line.strip().split()[1]
                if pkg in skip_pkgs:
                    skip = True
                    print(f'Skipping package: {pkg}')
                    skipped.append(pkg)
            if not skip:
                outf.write(line)
    skipped = sorted(skipped)

    env['CONDARC'] = op.abspath(op.join(basedir, '.condarc'))

    # In a FSL installation, the main FSL environment
    # is installed into the base miniconda environment,
    # and extras are created as child environments
    if envname is None:
        envdir = basedir
        action = 'update -n base'
    else:
        envdir = op.join(basedir, 'envs', envname)
        action = f'create -n {envname}'

    condabin = conda(basedir)

    with calc_progress_metrics(basedir, envdir) as results:
        sprun(f'{condabin} env {action} -f env.copy.yml',
              env=env, tee=True)
    # Add the skipped package count, so
    # the total npkgs is still accurate
    results.npkgs += len(skipped)
    results.sizes += sum(skip_pkgs[p] for p in skipped)

    print('--PACKAGES--')
    pkgs = sorted(set(results.end_pkgs).difference(results.start_pkgs))
    for i, p in enumerate(pkgs):
        print(i, p)
    print('--SKIPPED--')
    for i, s in enumerate(skipped, start=len(pkgs)):
        print(i, s)

    # The installer progress reporting mechanism
    # ("output") has undergone a few iterations.
    #   - version 1: monitor number of lines
    #                of standard output
    #   - version 2: monitor number of packages
    #                installed into $FSLDIR/pkgs/
    #   - version 3: monitor number of packages
    #                installed into $FSLDIR/pkgs/,
    #                and files installed into
    #                $FSLDIR/lib/ and $FSLDIR/bin/
    #   - version 4: monitor number of bytes of
    #                packages in $FSLDIR/pkgs/,
    #                and number of files installed
    #                into $FSLDIR/lib/ and
    #                $FSLDIR/bin/
    return {
        'version' : '4',
        'value' : {
            'pkgs'  : f'{results.npkgs}',
            'bin'   : f'{results.nbins}',
            'lib'   : f'{results.nlibs}',
            'size'  : f'{results.sizes}'
        }
    }


def fast_test(basedir, envfile, release_info, extra_pkgs, env):
    """Fast/dry-run test, performed to make sure that conda is able to resolve
    the FSL environment files.
    """

    # conda env update -f <file> doesn't have a dry run option,
    # so we use conda install --dry-run [pkg ...]
    packages = preprocess_environment(envfile, 'fsl', extra_pkgs)
    packages = ' '.join(f'"{p}"' for p in packages)
    cmd      = f'{conda(basedir)} install -p {basedir} -y --dry-run ' + packages

    # preprocess_environment creates a condarc file
    env['CONDARC'] = op.abspath(op.join('fsl', '.condarc'))

    sprun(cmd, env=env)

    return {
        'version' : '4',
        'value' : {
            'pkgs'  : f'{len(packages)}',
            'bin'   : '0',
            'lib'   : '0',
            'size'  : '0'
        }
    }


def install_miniconda(installer, destdir, is_micromamba):
    """Installs miniconda or micromamba."""

    if is_micromamba:
        with tarfile.open(installer) as f:
            f.extractall(destdir, filter='data')
            output = 1
    else:
        output = sprun(f'sh {installer} -b -p {destdir}', tee=True)
        output = len(output.split(b'\n'))

    return output


def run_miniconda_installations(release_info, platform):
    """Installs all miniconda/miniforge/micromamba installers for each python
    version and for this platform, returning information that can be added to
    the manifest (e.g. checksums, installation output line count).
    """

    minicondas = release_info['miniconda'][platform]
    info       = {}

    for pyver, url in minicondas.items():
        with tempdir():

            # installer is either a micromamba tarball
            download_file(url, 'miniconda.sh')
            checksum = sha256('miniconda.sh')
            output   = install_miniconda('miniconda.sh',
                                         './miniconda',
                                         pyver == 'micromamba')

            info[pyver]           = {}
            info[pyver]['url']    = url
            info[pyver]['output'] = str(output)
            info[pyver]['sha256'] = checksum

    return info


def main():
    platform_id   = sys.argv[1]
    release_file  = op.abspath(sys.argv[2])
    envinfo_file  = op.abspath(sys.argv[3])
    envdir        = op.abspath(sys.argv[4])
    variants      = sys.argv[5:]
    release_info  = load_release_info(release_file)
    all_platforms = get_platform_identifiers(release_info)

    # We may test an environment multiple times, according
    # to the "variants" passed on the command-line. Each
    # test results in a separate set of progress metrics;
    # the default test outputs are saved in the output
    # installation info with the key "default".
    #
    # The only variant currently recognised is "cuda=X.Y",
    # for which we trick conda into thinking that a GPU is
    # available. The results of the cuda test are saved
    # with the key "cuda=X.Y".
    variants = ['default'] + variants

    # Gather all environment files to be tested; make
    # sure that the main FSL environment file is first
    # (it will have the shortest file name), so that
    # the test installation mimics a real installation
    # (main env installed first, extra envs installed
    # afterwards)
    envfiles = list(glob.glob(f'{envdir}/*{platform_id}.yml'))
    envfiles = sorted(envfiles, key=len)

    if platform_id not in all_platforms:
        print(f'Platform {platform_id} is unrecognised or disabled '
               'by $FSLCONDA_PLATFORMS ({all_platforms})')
        sys.exit(0)

    full_test_override = 'RUN_FULL_TEST' in os.environ
    fast_test_override = 'RUN_FAST_TEST' in os.environ
    full_test_branches = os.environ.get('FULL_TEST_BRANCHES', '')
    branch_name        = os.environ.get('CI_COMMIT_BRANCH', None)
    is_tag             = 'CI_COMMIT_TAG' in os.environ
    run_full_test      = full_test_override or \
                           is_tag or ((branch_name is not None) and
                                      (branch_name in full_test_branches))
    run_full_test      = run_full_test and (not fast_test_override)

    # space-separated list of package names
    # to be skipped for the test (full test
    # only). Each name is of the form
    # "<package-name>=<size-in-bytes>"
    skip_pkgs = os.environ.get('SKIP_PACKAGES', '')
    if skip_pkgs != '':
        skip_pkgs = skip_pkgs.split(' ')
        skip_pkgs = [p.split('=') for p in skip_pkgs]
        skip_pkgs = {pkg : int(sz) for pkg, sz in skip_pkgs}

    # envinfo JSON structure passed to the generate_manifest
    # stage. Has the following structure:
    # {
    #    "platform"  : "<platform-id>",
    #    "miniconda" : {
    #        "micromamba" : {
    #            "url"    : "https://micromamba.tar.bz2",
    #            "sha256" : "abc",
    #            "output" : "10"               # used for progress reporting
    #        }
    #        "python3.10" : {
    #            # py310 miniconda installer, same fields as above
    #        },
    #        ...
    #    },
    #    # main environment file
    #    "env" : {
    #         "environment"  : "<envfile>.yml",
    #         "sha256"       : "abc",
    #         "cuda_enabled" : "false",           # Whether this environment
    #                                             # can use the conda-forge
    #                                             # CUDA stack
    #         "output"       : {"install" : ...}, # installation info used
    #                                             # for progress reporting
    #    },
    #    # extra/child environments
    #    "extras" : {
    #        "someenv" : {...}  # same structure as "env" above
    #    }
    # }
    envinfo = {
        'platform'  : platform_id,
        'miniconda' : run_miniconda_installations(release_info, platform_id),
        'env'       : {'output' : {}},
        'extras'    : {},
    }

    # Add an entry with basic info for each environment file.
    # Further installation info is added to each entry below.
    for envfile in envfiles:
        envname  = parse_environment_file_name(envfile)[1]
        fileinfo = {
            "sha256"      : sha256(envfile),
            "environment" : op.basename(envfile),
        }
        # main env or child env?
        if envname is None:

            # only on linux
            cuda_enabled = (platform_id == 'linux-64' and
                            'cuda' in release_info['packages'])
            fileinfo['cuda_enabled'] = str(cuda_enabled)
            envinfo['env'].update(fileinfo)
        else:

            # only on linux
            cuda_enabled = (platform_id == 'linux-64'  and
                            'cuda' in release_info['extras'][envname])
            fileinfo['output']         = {}
            fileinfo['cuda_enabled']   = str(cuda_enabled)
            envinfo['extras'][envname] = fileinfo

    # Perform a test installation for each environment
    # and for each installation environment variant.
    # Use a fixed conda package cache directory to
    # avoid downloading the same package more than
    # once.
    with tempdir(change=False) as pkg_cache_dir:

        env                   = clean_environ()
        env['CONDA_PKGS_DIR'] = pkg_cache_dir

        for variant in variants:

            with tempdir():

                # use a shared base environment for full tests,
                # so we can generate accurate progress reporting
                # metrics - in a FSL installation, all
                # environments (main+extras) will use the same
                # $FSLDIR/pkgs/ cache.
                basedir       = op.join(os.getcwd(), 'fsl')
                miniconda_url = release_info['miniconda'][platform_id]['micromamba']
                download_file(miniconda_url, 'micromamba.tar.bz2')
                install_miniconda('micromamba.tar.bz2', basedir, True)

                for envfile in envfiles:

                    envname = parse_environment_file_name(envfile)[1]

                    # main env or child env
                    if envname is None: destinfo = envinfo['env']['output']
                    else:               destinfo = envinfo['extras'][envname]['output']

                    # We currently just save progress info
                    # for a single CUDA version (key="cuda'),
                    # but may add info for multiple versions
                    # in the future if needed
                    if variant == 'default': variant_key = 'install'
                    else:                    variant_key = variant.split('=')[0]

                    # Only simulate CUDA when:
                    #  - installing an environment that has cuda listed
                    #    in its dependencies in the fsl-release.yml
                    #  - Running a "cuda=X.Y" variant test
                    if envname is None: has_cuda = 'cuda' in release_info['packages']
                    else:               has_cuda = 'cuda' in release_info['extras'][envname]

                    # Add CUDA to the package list,
                    # simulating fslinstaller behaviour
                    if has_cuda and variant.startswith('cuda'):

                        cuda_version = variant.split('=')[1]
                        major, minor = cuda_version.split('.')
                        pin          = f'>={major}.{minor},<{int(major) + 1}'
                        extra_pkgs   = [f'cuda-version{pin}']
                    else:

                        cuda_version = None
                        extra_pkgs   = None

                    # use a copy, as the full_test
                    # function may modify the file
                    shutil.copy(envfile, '.environment.yml')

                    with mock_nvidia_smi(cuda_version):

                        env['MAMBA_ROOT_PREFIX'] = basedir

                        if run_full_test:
                            print('Running full test...')
                            inst_info = full_test(basedir, '.environment.yml', envname,
                                                  release_info, skip_pkgs, extra_pkgs,
                                                  dict(env))
                        else:
                            # Note that the returned install_info
                            # dict for a fast test just contains
                            # dummy information
                            print('Running fast test...')
                            inst_info = fast_test(basedir, '.environment.yml',
                                                  release_info, extra_pkgs,
                                                  dict(env))

                    # The main environment may be tested twice
                    # - once for the default variant, and again
                    # for the cuda variant. But these will be
                    # equivalent as the main environment does
                    # not (currently) use any CUDA-capable
                    # packages. I haven't thought of a nice way
                    # to avoid running this twice (the second
                    # time is still needed for the child
                    # enviroment tests to complete correctly).
                    if variant_key not in destinfo:
                        if not (variant.startswith('cuda') and not has_cuda):
                            destinfo[variant_key] = inst_info

    envinfo = json.dumps(envinfo, indent=4)
    print(envinfo)

    envinfo_dir = op.dirname(envinfo_file)
    if not op.exists(envinfo_dir):
        os.mkdir(envinfo_dir)

    with open(envinfo_file, 'wt') as f:
        f.write(envinfo)


if __name__ == '__main__':
    sys.exit(main())
