#!/usr/bin/env python
#
# test_manifest.py - Run the fslinstaller script on a generated manifest file.

import os.path as op
import os
import glob
import sys
import json

from manifest_rules.utils import (sprun,
                                  tempdir,
                                  download_file,
                                  load_release_info,
                                  get_platform_identifiers,
                                  mock_nvidia_smi)


def smoke_tests(fsldir):
    env           = os.environ.copy()
    env['FSLDIR'] = fsldir
    kwargs = {'env' : env, 'tee' : True, 'text' : True}
    sprun(f'{fsldir}/share/fsl/bin/fslversion -v', **kwargs)
    sprun(f'{fsldir}/share/fsl/bin/fsleyes -V',    **kwargs)
    sprun(f'{fsldir}/share/fsl/bin/imglob image*', **kwargs)
    sprun(f'{fsldir}/share/fsl/bin/fslmaths -h',   **kwargs)


def main():
    platform        = sys.argv[1]
    release_file    = op.abspath(sys.argv[2])
    manifest_file   = op.abspath(sys.argv[3])
    environment_dir = op.abspath(sys.argv[4])
    extra_args      = sys.argv[5:]
    release_info    = load_release_info(release_file)
    platforms       = get_platform_identifiers(release_info)
    skip_pkgs       = os.environ.get('SKIP_PACKAGES', '')

    if skip_pkgs != '':
        skip_pkgs = skip_pkgs.split(' ')
        skip_pkgs = [p.split('=')[0] for p in skip_pkgs]
    else:
        skip_pkgs = []

    for p in platforms:
        if p == platform:
            break
    else:
        print('This platform is disabled')
        sys.exit(0)

    # fake a CUDA-capable environment?
    cuda_version = os.environ.get('CUDA_VERSION', None)

    # credentials for logging into
    # internal FSL conda channel
    username = os.environ['FSLCONDA_USERNAME']
    password = os.environ['FSLCONDA_PASSWORD']

    version  = os.environ['CI_COMMIT_TAG']

    with open(manifest_file, 'rt') as f:
        manifest = json.loads(f.read())

    installer_url = manifest['installer']['url']

    # patch the manifest to refer to local environment files
    for build in manifest['versions'][version]:
        envfname = build['environment'].split('/')[-1]
        envfname = op.join(environment_dir, envfname)
        build['environment'] = f'file://{envfname}'
        for extra in build.get('extras', {}).values():
            envfname = extra['environment'].split('/')[-1]
            envfname = op.join(environment_dir, envfname)
            extra['environment'] = f'file://{envfname}'

    manifest_file = op.join(op.dirname(manifest_file), 'manifest_patched.json')
    with open(manifest_file, 'wt') as f:
        manifest = json.dumps(manifest, indent=4, sort_keys=True)
        f.write(manifest)

    with tempdir(), mock_nvidia_smi(cuda_version):
        download_file(installer_url, 'fslinstaller.py')

        fsldir = op.abspath('fsl')

        os.makedirs('homedir', exist_ok=True)
        os.makedirs('workdir', exist_ok=True)

        args = f'--manifest {manifest_file} '

        sprun(f'{sys.executable} ./fslinstaller.py ' +
              args + '--listversions', text=True, tee=True)

        for pkg in skip_pkgs:
            args += f'--exclude_package {pkg} '

        try:
            args += f'--username {username} --password {password} ' + \
                    f'--dest {fsldir} --workdir workdir --homedir homedir'
            for extra in build.get('extras', {}).keys():
                args += f' --extra {extra}'
            args += ' ' + ' '.join(extra_args)

            sprun(f'{sys.executable} ./fslinstaller.py ' + args,
                  text=True, tee=True)

            smoke_tests(fsldir)

        finally:
            # Assuming there is only one log file
            logfile = glob.glob(op.join('workdir', 'fslinstaller*.log'))
            logfile = list(logfile)[0]
            if op.exists(logfile):
                print('Contents of fslinstaller.log file:')
                with open(logfile, 'rt') as f:
                    print(f.read())
            else:
                print('fslinstaller.log file was not created!')


if __name__ == '__main__':
    sys.exit(main())
