#!/usr/bin/env python
#
# Update the versions of all FSL packages in the fsl-release.yml manifest
# file.
#
# This script:
#   - queries the FSL conda channel to find the latest available versions
#     of all hosted packages
#   - queries anaconda.org to find the latest available versions of a
#     hard-coded set of externally hosted packages
#   - opens a merge request on the fsl/conda/manifest repository, updating
#     all package versions to the latest available.
#


import os
import re
import shlex
import sys
import fnmatch
import subprocess   as sp
import textwrap     as tw
import urllib.parse as urlparse

from manifest_rules.utils import (load_release_info,
                                  tempdir,
                                  sprun,
                                  indir,
                                  Version)
from fsl_ci.recipe        import  get_recipe_variable
from fsl_ci.conda         import  get_channel_packages
from fsl_ci.gitlab        import (open_merge_request,
                                  download_file,
                                  gen_branch_name,
                                  gen_repository_url,
                                  get_projects_in_namespace,
                                  http_request)


from packaging.version import parse as parse_version


EXTERNAL_PACKAGES = {
    'fslpy'              : 'conda-forge',
    'fsleyes-props'      : 'conda-forge',
    'fsleyes-widgets'    : 'conda-forge',
    'fsleyes'            : 'conda-forge',
    'fmrib-unpack'       : 'conda-forge',
    'file-tree'          : 'conda-forge',
    'fsl-pipe'           : 'conda-forge',
    'fsleyes-plugin-mrs' : 'conda-forge',
    'file-tree-fsl'      : 'conda-forge',
    'spec2nii'           : 'conda-forge',
}


def get_latest_package_versions(args, packages):
    """Retrieves latest available version of all specified packages from the
    given conda channel. Packages which are not on the channel are ignored.

    Returns a dict of {package : version} mappings, denoting the latest
    available version for each package
    """
    print(f'Downloading package versions from {args["channel_url"]}...')

    chanpkgs = get_channel_packages(**args)
    packages = [p for p in packages if p in chanpkgs]
    latest   = {}

    for pkgname in packages:
        # versions are ordered from oldest
        # to newest, so we want the last one
        version         = chanpkgs[pkgname][-1]
        latest[pkgname] = version.version

    return latest


def get_external_package_versions(packages):
    """Retrieves the latest available versions of each package
    in packages. Returns two dictionaries:
       - A {package : version} mapping for all packages in
         EXTERNAL_PACKAGES
       - A {package : version} mapping for all packages not in
         EXTERNAL_PACKAGES, assumed to be hosted on conda-forge
    """
    pkgvers = {}
    ignored = {}

    for package in packages:
        channel = EXTERNAL_PACKAGES.get(package, 'conda-forge')
        url     = f'https://api.anaconda.org/package/{channel}/{package}'

        try:
            meta   = http_request(url)

            # Files are ordered by release-date (oldest to newest).
            # Re-order them by version number, and take the highest.
            # We're ignoring cross-platform differences here.
            versions = [f['version'] for f in meta['files']]
            versions = sorted(versions, key=parse_version)
            pkgver   = versions[-1]

        # Not on channel?
        except Exception:
            pkgver = '??'

        if package in EXTERNAL_PACKAGES: pkgvers[package] = pkgver
        else:                            ignored[package] = pkgver

    return pkgvers, ignored


def get_latest_git_repository_tag(url):
    """Returns the latest tag on the remote git repository. Assumes that the
    repository tags are sensible version identifiers, e.g.  "X.Y.Z".
    """
    tags = sprun(f'git ls-remote --refs --tags {url}', text=True).split('\n')
    tags = [t for t in tags if 'refs/tags/' in t]
    tags = [t.split()[1].split('/')[-1] for t in tags]
    tags = [Version(t) for t in tags]
    tags = sorted(tags, reverse=True)
    return tags[0].verstr


def find_outdated_external_projects(packages, server, token):
    """Retrieves the latest tag on externally maintained projects (those with
    an external source code repository), and checks whether it matches the
    latest published conda package. Returns a dict of {name : version} mappings
    of packages where the latest tag does not match the latest package version.
    """
    outdated     = {}
    servername   = urlparse.urlparse(server).netloc
    all_packages = get_projects_in_namespace('fsl/conda', server, token)
    for package, version in packages.items():

        recipe_path = f'fsl/conda/{package}'

        if recipe_path not in all_packages:
            continue

        try:
            meta = download_file(recipe_path, 'meta.yaml', server, token)
        except Exception:
            print("Warning: Couldn't download meta.yaml "
                  f'file for package {recipe_path}')
            continue

        repo = get_recipe_variable(meta, 'repository')
        if repo is None:
            print("Warning: Couldn't identify project source "
                  f'repository from recipe {recipe_path}')
            continue

        # Project repository is hosted on our
        # internal `gitlab` - the recipe will
        # have been automatically updated, so
        # we don't need to consider it here
        if servername in repo:
            continue

        # latest tag does not match latest
        # conda package?
        latest = get_latest_git_repository_tag(repo)
        if latest != version:
            outdated[package] = latest

    return outdated


def get_candidate_packages(release_info):
    """Returns a {name : version} mapping of all packages listed in the
    fsl-release.yml file.
    """
    packages = release_info['packages']          + \
               release_info['linux-64-packages'] + \
               release_info['macos-64-packages'] + \
               release_info['macos-M1-packages']
    pkgvers = {}
    for pkg in packages:

        words = pkg.split(' ', maxsplit=1)
        name  = words[0]
        if len(words) == 1:
            ver = 'n/a'
        else:
            ver = words[1]
        pkgvers[name] = ver

    return pkgvers


def get_fslconda_channels(release_info):
    """Returns a list of FSL conda channels, read from the contents of
    fsl-release.yml.
    """

    channels = release_info['channels']
    channels = [c for c in channels
                if c not in ('conda-forge', 'defaults', 'nodefaults')]

    # prepare channels to be passed as kwargs
    # to fsl_ci.conda.read_channel_repodata
    channels = [{'channel_url' : c} for c in channels]

    internal = release_info['internal-channel']
    # internal channel refers to ${FSLCONDA_USERNAME}/PASSWORD,
    # but python http lib won't resolve such a url.
    internal = internal.replace('${FSLCONDA_USERNAME}:${FSLCONDA_PASSWORD}@', '')

    internal = {'channel_url' : internal,
                'username'    : os.environ['FSLCONDA_USERNAME'],
                'password'    : os.environ['FSLCONDA_PASSWORD']}

    return [internal] + channels


def versions_equal(v1, v2):
    """Compare two version strings, returning True if they are equal."""

    # fsl-release.yml may contain partially pinned versions,
    # e.g. "0.7.*". If the latest version matches the pattern,
    # we don't need to update it. We also ignore leading 'v's,
    # as some projects have tags both with and without them.
    v1 = v1.lower().lstrip('v')
    v2 = v2.lower().lstrip('v')
    return fnmatch.fnmatch(v1, v2)


def update_release_file_packages(pkg_versions):
    """Updates all package versions in the fsl-release.yml file. """

    # We don't use pyyaml here, because we
    # want to preserve comments/formatting/etc
    with open('fsl-release.yml', 'rt') as f:
        lines = list(f.readlines())

    print('Updating package versions in fsl-release.yml...')

    # Very cheap and un-informed method of updating
    # "packages" and "<platform>-packages" lists,
    # but *not* "build-packages"
    for pkg, ver in pkg_versions.items():
        for i, line in reversed(list(enumerate(lines))):
            line  = line.rstrip()
            pat   = rf'^( +)-( *){pkg}( .*)?$'
            match = re.fullmatch(pat, line)
            if match:
                oldver = line.split()[-1]
                if not versions_equal(ver, oldver):
                    space1   = match.group(1)
                    space2   = match.group(2)
                    lines[i] = f'{space1}-{space2}{pkg} {ver}\n'
                break

    with open('fsl-release.yml', 'wt') as f:
        f.writelines(lines)


def update_release_file_fslinstaller_version(packages):
    """Updates the fslinstaller version number listed in teh FSL release
    manifest. The version number is taken from the latest available
    fsl-installer package on the public FSL conda channel.
    """
    with open('fsl-release.yml', 'rt') as f:
        lines = list(f.readlines())

    instver = packages['fsl-installer']

    for i, line in list(enumerate(lines)):
        line  = line.rstrip()
        pat   = r'^installer:.*$'
        match = re.fullmatch(pat, line)
        if match:
            lines[i] = f'installer: {instver}\n'
            break

    with open('fsl-release.yml', 'wt') as f:
        f.writelines(lines)


def commit_changes_and_open_mr(repo_url, server, token, target_branch,
                               ignored, outdated):
    """Creates a new branch, commits any changes made by update_release_file,
    pushes the branch to gitlab, then opens a MR.
    """

    branch = gen_branch_name('mnt/update-all-packages',
                             'fsl/conda/manifest',
                             server, token)
    mrmsg = tw.dedent("""
    This merge request was manually triggered by the update-all-packages
    CI job on the fsl/conda/manifest gitlab repository.

    This MR updates all FSL packages to their latest available versions.

    (MR automatically generated by fsl/conda/manifest-rules running on the
    fsl/conda/manifest repository).
    """).strip()

    if len(outdated) > 0:
        mrmsg += '\n\n**Important:** The following externally maintained ' \
                 'projects may have new versions, and their conda recipes ' \
                 'may need updating:'
        for pkg, oldver, newver in outdated:
            mrmsg += f'\n - `{pkg}` (`{oldver}` -> `{newver}`)'

    if len(ignored) > 0:
        mrmsg += '\n\nThe following externally hosted packages ' \
                 'were not considered in this update:'
        for pkg, oldver, newver in ignored:
            mrmsg += f'\n - `{pkg}` (`{oldver}` -> `{newver}`)'

    print('Merge request message:')
    print(mrmsg)

    changed = sp.run(shlex.split('git diff --exit-code fsl-release.yml'))
    if changed.returncode == 0:
        print('All package versions in fsl-release.yml are up to date - exiting')
        sys.exit(0)

    sprun( 'git config --global user.name manifest-rules')
    sprun( 'git config --global user.email manifest-rules@git.fmrib.ox.ac.uk')
    sprun(f'git remote add origin_auth {repo_url}')
    sprun( 'git fetch  --all')
    sprun(f'git checkout -b {branch}')
    sprun( 'git add fsl-release.yml')
    sprun( 'git commit -m "MNT: Update all FSL package '
           'versions [fsl/conda/manifest-rules]"')
    sprun(f'git push origin_auth {branch}')
    open_merge_request('fsl/conda/manifest', branch, mrmsg, server, token,
                       target_branch=target_branch)


def main():

    server        = os.environ['CI_SERVER_URL']
    token         = os.environ['FSL_CI_API_TOKEN']
    target_branch = os.environ['CI_COMMIT_BRANCH']
    repo_url      = gen_repository_url('fsl/conda/manifest', server, token)

    release_info  = load_release_info('fsl-release.yml')
    pkgs          = get_candidate_packages(release_info)
    channels      = get_fslconda_channels(release_info)

    # Get info about packages which
    # are on the FSL conda channel(s).
    pkgvers = {}
    for chan in channels:
        pkgvers.update(get_latest_package_versions(chan, pkgs.keys()))

    # Get info about packages which are
    # not on any of the FSL conda
    # channels, and so are probably on
    # conda-forge. Any external packages
    # not considered for updating are
    # placed into ignored.
    externs          = [pkg for pkg in pkgs if pkg not in pkgvers]
    externs, ignored = get_external_package_versions(externs)

    pkgvers.update(externs)

    # Recipes for internally developed
    # projects (e.g. fsl/newimage) are
    # automatically updated through
    # gitlab CI. Recipes for internally
    # built, but externally developed
    # projects (e.g. github/washu/ciftilib)
    # need to be manually updated
    outdated = find_outdated_external_projects(pkgs, server, token)

    # make ignored/outdated lists contain
    # (pkg, oldver, newver) tuples
    ignored  = [(i[0], pkgs[i[0]], i[1]) for i in ignored.items()]
    outdated = [(i[0], pkgs[i[0]], i[1]) for i in outdated.items()]

    update_release_file_packages(pkgvers)
    update_release_file_fslinstaller_version(pkgvers)
    commit_changes_and_open_mr(repo_url, server, token, target_branch,
                               ignored, outdated)


if __name__ == '__main__':
    sys.exit(main())
