#!/usr/bin/env python
#
# Updates the version of the fslinstaller script in the release manifest.

import copy
import json
import os
import os.path as op
import sys
import shutil
import re

from manifest_rules.utils import sha256, sprun, tempdir


def extract_fslinstaller_version(fslinstaller_path):
    with open(fslinstaller_path, 'rt') as f:
        for line in f:
            if line.startswith('__version__'):
                version = line.strip().split()[-1].strip("'")
                break
    return version


def main():
    releasedir     = op.abspath(sys.argv[1])
    manifest_path  = os.environ['MANIFEST_PATH']
    installer_path = os.environ['INSTALLER_PATH']
    manifest_path  = op.join(releasedir, manifest_path)
    installer_path = op.join(releasedir, installer_path)
    dry_run        = os.environ['DRY_RUN'].lower() != "false"

    if not op.exists(manifest_path):
        print(f'Manifest file does not exist! {manifest_path}')
        return 1

    if not op.exists(installer_path):
        print(f'Installer file does not exist! {installer_path}')
        return 1

    new_checksum = sha256(installer_path)
    new_version  = extract_fslinstaller_version(installer_path)

    # move manifest.json to manifest.backup.json
    base, ext   = op.splitext(manifest_path)
    backup_path = f'{base}.backup{ext}'
    print(f'Moving {manifest_path} to {backup_path} [dry_run: {dry_run}]')

    if not dry_run:
        shutil.copy(manifest_path, backup_path)

    # update manifest contents
    contents                         = open(manifest_path, 'rt').read()
    contents                         = json.loads(contents)
    oldcontents                      = copy.deepcopy(contents)
    contents['installer']['version'] = new_version
    contents['installer']['sha256']  = new_checksum
    contents                         = json.dumps(contents,    indent=4)
    oldcontents                      = json.dumps(oldcontents, indent=4)

    print('----------')
    print(contents)
    print('----------')
    print()

    with tempdir():
        with open('old.json', 'wt') as f: f.write(oldcontents)
        with open('new.json', 'wt') as f: f.write(contents)
        print('Change diff:')
        print('----------')
        print(sprun(f'diff old.json new.json', check=False, text=True))
        print('----------')

    print(f'Writing new manifest contents to '
          f'{manifest_path} [dry_run: {dry_run}]')
    if not dry_run:
        with open(manifest_path, 'wt') as f:
            f.write(contents)


if __name__ == '__main__':
    sys.exit(main())
