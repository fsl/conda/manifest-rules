#!/usr/bin/env python
# TODO change #output lines to characters?
#
#
# utils.py - Miscellaneous utility functions.


import                    contextlib
import functools       as ft
import                    glob
import                    hashlib
import                    os
import os.path         as op
import                    re
import                    shlex
import                    shutil
import subprocess      as sp
import                    string
import                    tempfile
import textwrap        as tw
from   unittest    import mock
import urllib.parse    as urlparse
import urllib.request  as urlrequest

try:                import yaml
except ImportError: yaml = None

try:                import dateutil.parser as dateutil
except ImportError: dateutil = None

try:                from fsl_ci.gitlab import lookup_project_tags
except ImportError: lookup_project_tags = None


def sprun(cmd, **kwargs):
    """Runs the given command with subprocess.run, returning its standard
    output as a byte string (this can be overridden by passing text=True).
    """
    print(f'Running: {cmd}')
    cmd    = shlex.split(cmd)
    text   = kwargs.get('text',  False)
    check  = kwargs.pop('check', True)
    tee    = kwargs.pop('tee',   False)
    proc   = sp.Popen(cmd, stdout=sp.PIPE, **kwargs)

    if text: stdout = ''
    else:    stdout = b''

    while True:
        line = proc.stdout.readline()
        if line in ('', b''):
            break
        stdout += line
        if tee:
            if text: print(line.strip())
            else:    print(line.strip().decode('utf-8'))

    returncode = proc.wait()

    if check and (returncode != 0):
        if not text:
            stdout = stdout.decode('utf-8')

        err  = f'Called process returned {returncode}: {cmd}\n'
        if not tee:
            err += f'standard output:\n{stdout}\n'
        print(err, flush=True)

        raise RuntimeError(err)
    return stdout


def clean_environ(keep_auth=True):
    """Return a dict containing a set of sanitised environment variables.

    All FSL and conda related variables are removed.
    """
    env = os.environ.copy()
    for v in list(env.keys()):
        if keep_auth and (v in ('FSLCONDA_USERNAME', 'FSLCONDA_PASSWORD')):
            continue

        # These are the only conda env vars
        # we allow (see manifest-gitlab-ci.yml,
        # and the mock_nvidia_smi function
        # down below)
        if v in ('CONDA_OVERRIDE_CUDA', 'CONDA_OVERRIDE_OSX'):
            continue
        if any(('FSL' in v, 'CONDA' in v)):
            env.pop(v)
    return env


@ft.total_ordering
class Version:
    def __init__(self, verstr):

        self.verstr = verstr

        if verstr.lower().startswith('v'):
            verstr = verstr[1:]
        # Version identifiers for official FSL
        # releases will have up to four
        # components (X.Y.Z.W), but We accept
        # any number of (integer) components,
        # as internal releases may have more.
        components = []

        for comp in verstr.split('.'):
            try:              components.append(int(comp))
            except Exception: break

        self.components = components

    def __str__(self):
        return self.verstr

    def __eq__(self, other):
        for sn, on in zip(self.components, other.components):
            if sn != on:
                return False
        return len(self.components) == len(other.components)

    def __lt__(self, other):
        for p1, p2 in zip(self.components, other.components):
            if p1 < p2: return True
            if p1 > p2: return False
        return len(self.components) < len(other.components)


def get_most_recent_release():
    """Returns the version identifier of the most recent public FSL release.
    """

    # get tag list from gitlab if possible
    project = os.environ.get('CI_PROJECT_PATH',  None)
    server  = os.environ.get('CI_SERVER_URL',    None)
    token   = os.environ.get('FSL_CI_API_TOKEN', None)

    if all ((project is not None,
             server  is not None,
             token   is not None)):
        tags = lookup_project_tags(project, server, token)

    else:
        tags = sprun('git tag --list', text=True).split('\n')
        tags = [t.strip() for t in tags]

    tags = [Version(t) for t in tags if t != '']
    tags = sorted(tags)

    # 6.0.5 was the last non-conda-based FSL release
    if len(tags) == 0: return '6.0.5'
    else:              return str(tags[-1])


def generate_development_version_identifier():
    """Generate a version string for a development/internal FSL release.

    The version string incorporates the current git commit hash, branch name,
    and date.
    """

    # get info about the current branch/commit/date.
    # Use gitlab CI vars if possible, fall back to
    # git call-out
    tag    = get_most_recent_release()
    commit = os.environ.get('CI_COMMIT_SHORT_SHA', None)
    branch = os.environ.get('CI_COMMIT_BRANCH',    None)
    date   = os.environ.get('CI_COMMIT_TIMESTAMP', None)

    if commit is None:
        commit = sprun('git rev-parse --short      HEAD', text=True).strip()
    if branch is None:
        branch = sprun('git rev-parse --abbrev-ref HEAD', text=True).strip()
    if date is not None:
        date = dateutil.isoparse(date)
        date = date.strftime('%Y%m%d')
    else:
        date = sprun('git show -s --format=%cd --date=format:"%Y%m%d" HEAD',
                     text=True).strip()

    # make usable as part of a file name,
    # and make sure no underscores, as we
    # use them as separators in env file
    # names
    branch = re.sub(r'[^a-zA-Z0-9]', '-', branch.lower())

    return f'{tag}.{date}.{commit}.{branch}'


def generate_environment_file_name(version, platform, env_name=None):
    """Generate a file name for the FSL conda environment file
    for the specified version and platform. If version is None,
    a development version identifier is generated.
    """

    allowed = string.ascii_letters + string.digits + '-.'
    for component in (version, platform, env_name):
        if component is None:
            continue
        if any(c not in allowed for c in component):
            raise RuntimeError(
                'File name components may only contain letters, '
                f'numbers, periods and hyphens: {env_name}')

    if version is None:
        version = generate_development_version_identifier()

    if env_name is None:
        return f'fsl-{version}_{platform}.yml'
    else:
        return f'fsl-{version}_{env_name}_{platform}.yml'


def parse_environment_file_name(filename):
    """Parses the given FSL environment file name, returning the
    version, platform, and environment name. The environment name
    will be None if the file name does not contain one.
    """

    pat      = r'fsl-([^_]+)(?:_([^_]+))?_([^_]+)\.yml'
    filename = op.basename(filename)
    match    = re.fullmatch(pat, filename)

    if match is not None: return match.groups()
    else:                 return (None, None, None)


def load_release_info(fsl_release_file):
    """Loads fsl-release.yml, returning its contents as a dict. """
    with open(fsl_release_file, 'rt') as f:
        return yaml.load(f.read(), Loader=yaml.Loader)


def get_platform_identifiers(release_info):
    """Returns a list of all platform identifiers for which FSL environment
    files are being generated, e.g. ['linux-64', 'macos-64', 'macos-M1'].
    """
    platforms  = os.environ.get('FSLCONDA_PLATFORMS', None)
    if platforms is None:
        platforms = list(release_info['miniconda'].keys())
    else:
        platforms = platforms.split(',')
    return platforms


@contextlib.contextmanager
def tempdir(change=True):
    """Context manager which creates, changes into, and returns a
    temporary directory, and then deletes it on exit.
    """

    tmpdir  = tempfile.mkdtemp()
    prevdir = os.getcwd()

    try:
        if change:
            os.chdir(tmpdir)
        yield tmpdir

    finally:
        if change:
            os.chdir(prevdir)
        shutil.rmtree(tmpdir)


@contextlib.contextmanager
def indir(dest):
    """Context manager which changes into, then out of, a specified directory.
    """

    prevdir = os.getcwd()

    try:
        os.chdir(dest)
        yield

    finally:
        os.chdir(prevdir)


@ft.cache
def download_data(url, blocksize=1048576):
    """Download and return data from url. """

    # support local files
    if op.exists(url):
        with open(url, 'rb') as f:
            return f.read()

    print(f'Downloading {url}')
    data = b''
    headers = {'User-Agent': 'Mozilla/5.0'}
    req     = urlrequest.Request(url, headers=headers)
    with urlrequest.urlopen(req) as req:
        while True:
            block = req.read(blocksize)
            if len(block) == 0:
                break
            data += block

    return data


def download_file(url, destination, blocksize=1048576):
    """Download a file from url, saving it to destination. """
    data = download_data(url, blocksize)
    with open(destination, 'wb') as f:
        f.write(data)


def sha256(filename):
    """Calculate the SHA256 checksum of the given file. """
    blocksize = 1048576
    hashobj   = hashlib.sha256()
    with open(filename, 'rb') as f:
        while True:
            block = f.read(blocksize)
            if len(block) == 0:
                break
            hashobj.update(block)
    return hashobj.hexdigest()



def create_devrelease_index(releasedir):
    """Build devreleases.txt - an index of all manifest files corresponding to
    development releases, hosted at
    https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/devreleases.txt
    """

    relurl = os.environ['FSL_RELEASE_URL']

    # The public release manifest is called "manifest.json",
    # and release backup manifests are called "manifest-<tag>.json"
    # (e.g. "manifest-6.0.6.2.json").
    #
    # Development release manifests are called
    # "manifest-<tag>.<date>.<commit>.<branch>.json".

    # We're only interested in dev releases
    def is_devrelease(fname):
        return len(fname.split('.')) > 2

    # We sort them by date, newest first
    def getdate(fname):
        fname = fname.lstrip('manifest-').rstrip('.json')
        return fname.split('.')[1]

    devreleases = glob.glob(op.join(releasedir, 'manifest-*.json'))
    devreleases = [op.basename(r) for r in devreleases]
    devreleases = [r for r in devreleases if is_devrelease(r)]
    devreleases = sorted(devreleases, key=getdate, reverse=True)

    with open(op.join(releasedir, 'devreleases.txt'), 'wt') as f:
        for r in devreleases:
            f.write(urlparse.urljoin(relurl, r) + '\n')


@contextlib.contextmanager
def mock_nvidia_smi(cuda_version):
    """Injects a dummy "nvidia-smi" command into the environment, and set
    CONDA_OVERRIDE_CUDA to trick conda into thinking that a GPU is available
    (or is not available, if cuda_version is None).
    """

    if cuda_version is None:
        with mock.patch.dict(os.environ,
                             CONDA_OVERRIDE_CUDA=''):
            yield
        return

    with tempdir(change=False) as td:

        filepath = op.join(td, 'nvidia-smi')
        contents = tw.dedent(f"""
        #!/usr/bin/env bash
        echo "CUDA Version: {cuda_version}"
        exit 0
        """).strip()

        with open(filepath, 'wt') as f:
            f.write(contents)
        os.chmod(filepath, 0o755)

        path = op.pathsep.join((td, os.environ['PATH']))

        with mock.patch.dict(os.environ,
                             PATH=path,
                             CONDA_OVERRIDE_CUDA=cuda_version):
            yield
