#!/usr/bin/env python


import os.path as op

from setuptools import setup, find_packages


basedir = op.dirname(__file__)


# fsl-ci-rules can be installed beforehand from
# https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules
requirements = ['fsl-ci-rules', 'pyyaml', 'python-dateutil', 'packaging']

version = {}
with open(op.join(basedir, 'manifest_rules', '__init__.py')) as f:
    for line in f:
        if line.startswith('__version__ = '):
            exec(line, version)
            break
version = version['__version__']


setup(
    name='fsl-manifest-rules',
    version=version,
    description='Utilities for generating FSL release manifests',
    url='https://git.fmrib.ox.ac.uk/fsl/conda/manifest-rules',
    author='Paul McCarthy',
    author_email='paul.mccarthy@ndcn.ox.ac.uk',
    install_requires=requirements,
    packages=find_packages(),
    include_package_data=True,
    entry_points={'console_scripts' : [
        'clear_release_files        = manifest_rules.clear_release_files:main',
        'deploy_files               = manifest_rules.deploy_files:main',
        'generate_changelog         = manifest_rules.generate_changelog:main',
        'generate_environment_files = manifest_rules.generate_environment_files:main',
        'generate_manifest_file     = manifest_rules.generate_manifest_file:main',
        'replace_manifest           = manifest_rules.replace_manifest:main',
        'test_environment           = manifest_rules.test_environment:main',
        'test_manifest              = manifest_rules.test_manifest:main',
        'update_all_packages        = manifest_rules.update_all_packages:main',
    ]}
)
